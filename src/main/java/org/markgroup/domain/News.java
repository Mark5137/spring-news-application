package org.markgroup.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "news")
public class News {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "date_publish")
    private Date datePublish;

    @Column(name = "date_update")
    private Date dateUpdate;

    @Column(name = "like")
    private int like;

    @Column(name = "dislike")
    private int dislike;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="theme_id")
    private Theme theme;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="tag_id")
    private Tag tag;

}
