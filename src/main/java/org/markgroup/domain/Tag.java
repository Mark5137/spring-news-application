package org.markgroup.domain;

import javax.persistence.*;

@Entity
@Table(name = "tags")
public class Tag {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "tag_name")
    private String tagName;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "tag")
    private News news;

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
