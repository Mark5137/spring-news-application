package org.markgroup.domain;

import javax.persistence.*;

@Entity
@Table(name = "theme")
public class Theme {
    @Id @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "theme_name")
    private int themeName;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "theme")
    private News news;

    public Theme(int themeName) {
        this.themeName = themeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getThemeName() {
        return themeName;
    }

    public void setThemeName(int themeName) {
        this.themeName = themeName;
    }
}
